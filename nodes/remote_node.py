#!/usr/bin/env python
import rospy
from pynput import keyboard
from std_msgs.msg import String

def on_press(key):
    rospy.loginfo(key)
#     try:
#         rospy.loginfo('\n alphanumeric key {0} pressed'.format(
#             key.char))
#     except AttributeError:
#         rospy.loginfo('\n special key {0} pressed'.format(
#             key))

def on_release(key):
    rospy.loginfo('\n {0} released'.format(
        key))
    if key == keyboard.Key.esc:
        # Stop listener
        return False

def keyboard_listener():
    rospy.init_node('keyboard_listener', anonymous=True)
    listener = keyboard.Listener(
        on_press=on_press,
        on_release=on_release)
    listener.start()
    rospy.loginfo('ready to read keyboard inputs')
    r = rospy.Rate(100) # 10hz
    while not rospy.is_shutdown() and listener.running:
        r.sleep()

if __name__ == '__main__':
    keyboard_listener()
